set term postscript eps enhanced color
set output "../report/autos.eps" 
set size 0.8, 0.8

set xlabel "Shift"
set ylabel "Pitch[Hz]"
set logscale y
set grid xtics ytics mxtics mytics
set key bottom # 凡例

p "./pitches.out" title "Pitches" w lp
