/**
* Covert binary data to text data.
* @author Tatsuya Iwanari
* ver.1.0 2013/10/17
*/

#include <stdio.h>
#include <stdlib.h>

#define f 16000

void err(char *str)
{
	fprintf(stderr, str);
	exit(1);
}

int main(int argc, char *argv[])
{
	int i, framelen;
	short *sdata;
	char err_str[200];
	FILE *fp;

	if (argc != 3)
	{
		sprintf(err_str, "Usage: %s DATfile frame_length[sample]\n", argv[0]);
		err(err_str);
	}

	if ((fp = fopen(argv[1], "rb")) == NULL)
	{
		sprintf(err_str, "Cannot open %s\n", argv[1]);
		err(err_str);

	}
	if ((framelen = atoi(argv[2])) < 0)
	{
		sprintf(err_str, "Frame length %s is not positive.", argv[2]);
		err(err_str);
	}

	if ((sdata = (short *)malloc(sizeof(short) * framelen)) == NULL)
		err("Failed to memory allocate\n");

	fread(sdata, sizeof(short), framelen, fp);
	fclose(fp);

	for (i = 0; i < framelen; i++)
		printf("%f %d \n", (double)i / f, sdata[i]);

	return 0;
}