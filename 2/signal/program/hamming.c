/**
* Covert binary data to text data.
* Output data will be multiplied by hamming window.
* @author Tatsuya Iwanari
* ver.1.0 2013/10/17
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define f 16000
#define N 512

#define Hamming(a, b) (0.54 - 0.46 * cos(2 * (a) * M_PI / ((b) - 1)))

void err(char *str)
{
	fprintf(stderr, str);
	exit(1);
}

int main(int argc, char *argv[])
{
	int i;
	short *sdata;
	char err_str[200];
	FILE *fp;

	if (argc != 2)
	{
		sprintf(err_str, "Usage: %s DATfile\n", argv[0]);
		err(err_str);
	}

	if ((fp = fopen(argv[1], "rb")) == NULL)
	{
		sprintf(err_str, "Cannot open %s\n", argv[1]);
		err(err_str);

	}

	if ((sdata = (short *)malloc(sizeof(short) * N)) == NULL)
		err("Failed to memory allocate\n");

	fread(sdata, sizeof(short), N, fp);
	fclose(fp);

	for (i = 0; i < N; i++)
		printf("%f %d \n", (double)i / f, (int) (sdata[i] * Hamming(i, N)));

	return 0;
}