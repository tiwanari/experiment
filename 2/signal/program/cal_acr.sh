#!/bin/sh
# $B0z?t$rA*$V(B
if [ $# -ne 1 ]; then
	echo "Usage: $0 Datfile"
	exit 1
fi

> pitches.out
for n in `seq 0 20`
do
	nskip=`expr $n \* 256`
	echo `./autos $1 ${nskip}` >>pitches.out 
done
