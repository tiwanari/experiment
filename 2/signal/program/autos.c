/**
* Culculate autocorrelations by skipping.
* Input data will be multiplied by hamming window.
* @author Tatsuya Iwanari
* ver.1.0 2013/10/17
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define F 16000.0
#define N 512
#define THRESHOLD_RATE 1.2

#define Hamming(a, b) (0.54 - 0.46 * cos(2 * (a) * M_PI / ((b) - 1)))

void err(const char *str)
{
	fprintf(stderr, str);
	exit(1);
}

int main(int argc, char *argv[])
{
	int i, j, npeek = 0;

	int nskip;
	short *sdata;
	char err_str[200];

	double r_0, r_t, *rs;
	int *peek;
	double max;
	int max_i;
	
	FILE *fp;

	if (argc != 3)
	{
		sprintf(err_str, "Usage: %s DATfile skip\n", argv[0]);
		err(err_str);
	}

	if ((fp = fopen(argv[1], "rb")) == NULL)
	{
		sprintf(err_str, "Cannot open %s\n", argv[1]);
		err(err_str);
	}

	if ((nskip = atoi(argv[2])) < 0)
	{
		sprintf(err_str, "Skip length %s is not positive.", argv[2]);
		err(err_str);
	}

	if ((sdata = (short *)malloc(sizeof(short) * N)) == NULL
		|| (rs = (double *)malloc(sizeof(double) * N)) == NULL
		|| (peek = (int *)malloc(sizeof(int) * N)) == NULL)
	{
		err("Failed to memory allocate\n");	
	}

	fseek(fp, nskip * sizeof(short), SEEK_SET);
	fread(sdata, sizeof(short), N, fp);
	fclose(fp);


	// Multiplid by Hamming
	r_0 = 0.0;
	for (i = 0; i < N; i++)
	{
		sdata[i] = sdata[i] * Hamming(i, N);
		r_0 += sdata[i] * sdata[i];
	}
		
	// Calculate autocorrelations
	for (i = 0; i < N; i++)
	{
		r_t = 0;
		for (j = 0; j < N - i; j++)
		{
			r_t += sdata[j] * sdata[j + i];
		}
		rs[i] = r_t / r_0;
	}

	// List up all peeks
	for (i = 1; i < N - 1; i++)
	{
		if (rs[i] > rs[i-1] && rs[i] > rs[i+1])
		{
			peek[npeek++] = i;
			// printf("peek = %d, f = %f, rs[i] = %f\n", i, F / i, rs[i]);
		}
	}

	// Identify the pitch
	max = 1.0;
	max_i = -1;
	for (i = 1; i < npeek - 1; i++)
	{
		if (rs[peek[i]] / rs[peek[i-1]] > THRESHOLD_RATE
			&& rs[peek[i]] / rs[peek[i+1]] > THRESHOLD_RATE)
		{
			if (max_i == -1 || rs[max_i] < rs[peek[i]])
			{
				max_i = peek[i];
			}
		}
	}

	if (max_i != -1)
	{
		printf("%d %f\n", nskip, F / max_i);
	}

	free(sdata);
	free(rs);
	free(peek);

	return 0;
}