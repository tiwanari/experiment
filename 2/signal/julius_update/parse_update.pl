#! /usr/bin/perl

use encoding "cp932";

$| = 1;
while( <STDIN> ){
    if( s/^sentence1: // ){
      if (/AISATSU/) {    #挨拶
        print "1\n";
      } elsif (/HO-MU/) { #ホームポジション
        print "2\n";
      } elsif (/BAIBAI/) { #手を振る
        print "3\n";
      } elsif (/EIEIO-/) { #エイエイオー
        print "4\n";
      } elsif (/KUYASHIGARU/) { #悔しがる
        print "5\n";  
      } elsif (/SAKADACHI/) { #逆立ち
        print "6\n";
      } elsif (/HAKUSHU2/) { #拍手ルーチン
        print "7\n";
      } elsif (/HAKUSHU/) { #拍手
        print "8\n";
      } elsif (/O-EN/) { #三三七拍子
        print "9\n";
      } elsif (/UDETATE/) { #腕立て伏せ
        print "10\n";
      } elsif (/KUSSHIN/) { #片足屈伸
        print "11\n";
      } elsif (/USAGI/) { #うさぎ跳び
        print "12\n";
      } elsif (/OKIRU_J/) { #起き上がり（方向判定）
        print "13\n";
      } elsif (/OKIRU_U/) { #起きあがり（うつぶせ）
        print "14\n";
      } elsif (/OKIRU_A/) { #起き上がり（あおむけ）
        print "15\n";
      } elsif (/ZENSHIN/) { #微小歩行（前進五歩）
        print "16\n";
      } elsif (/KOUTAI/) { #微小歩行 (後進五歩)
        print "17\n";
      } elsif (/ARUKI_L/) { #微小歩行 (左サイトカウンタ五歩)
        print "18\n";
      } elsif (/ARUKI_R/) { #微小歩行 (右サイトカウンタ五歩)
        print "19\n";
      } elsif (/MAWARU_L/) { #標準旋回（左カウンタ五回）
        print "20\n";
      } elsif (/MAWARU_R/) { #標準旋回（右カウンタ五回）
        print "21\n";
      } elsif (/HASHIRUf/) { #標準歩行B(前進カウンタ五歩）
        print "22\n";
      } elsif (/HASHIRUb/) { #標準歩行B(後進カウンタ五歩)
        print "23\n";
      } elsif (/HASHIRU_L/) { #標準歩行（左サイトカウンタ五歩）
        print "24\n";
      } elsif (/HASHIRU_R/) { #標準歩行（右サイトカウンタ五歩）
        print "25\n";
      } elsif (/KICKf_L/) { #ボールを前に蹴る(左脚)
        print "26\n";
      } elsif (/KICKf_R/) { #ボールを前に蹴る(右足)
        print "27\n";
      } elsif (/KICK_L/) { #ボールを横に蹴る(左脚)
        print "28\n";
      } elsif (/KICK_R/) { #ボールを横に蹴る(右足)
        print "29\n";
      } elsif (/KICKb_R/) { #ボールを後ろに蹴る(左脚)
        print "30\n";
      } elsif (/KICKb_L/) { #ボールを後ろに蹴る(右足)
        print "31\n";
      } elsif (//) {
        print "32\n";
      } elsif (/SHAGAMU/) { #しゃがむ
        print "33\n"; 
      } elsif (/PUNCHf/) { #前パンチ
        print "34\n";
      } elsif (/URAKEN_L/) { #裏拳(左)
        print "35\n";
      } elsif (/URAKEN_R) { #裏拳(右)
        print "36\n";
      } elsif (/PUNCJ_L/) { #横パンチA(左)
        print "37\n";
      } elsif (/PUNCH_R/) { #横パンチA(右)
        print "38\n";
      }
    }
}

